//
//  ViewController.swift
//  shoppingList
//
//  Created by Rajani Kaparthy on 2017-09-12.
//  Copyright © 2017 Rajani Kaparthy. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    
    var ref : DatabaseReference!
    @IBOutlet weak var fruitText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //reference for database
        
        ref = Database.database().reference()
        
        //save in the database -write to db
        //self.ref.child("pia7").child("morestuff").setValue(["fruit": "apelsin", "color":"orange"])

        //receive data - read from db
        ref.child("pia7").child("morestuff").observeSingleEvent(of: .value, with:
            { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary //key value pair
         //   let username = value?["username"] as? String ?? ""
           // let user = User.init(username: username)
            let fruittext = value?["fruit"] as? String ?? ""
            let colortext = value?["color"] as? String ?? ""
            print(fruittext)
            print(colortext)
            // ...
                
                self.fruitText.text = fruittext
                
                
        }) { (error) in
            print(error.localizedDescription)
        }


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func saveIn(_ sender: UIButton) {
        
        self.ref.child("pia7").child("morestuff").setValue(["fruit": fruitText.text])
        
    }


}

